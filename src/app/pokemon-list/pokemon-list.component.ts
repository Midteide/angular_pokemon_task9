import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { DataService } from '../data.service';




@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})

export class PokemonListComponent implements OnInit {
  pokemonsData:any[] = [];

  constructor(private dataService: DataService) { 
    
  }

  ngOnInit() {

    this.getMorePokemons();

    // this.dataService.sendGetRequest().subscribe((data: any[])=>{
    //   console.log(data);
    //   this.pokemonsData = data.results;
    // })
    
    //console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!",this.pokemonsData)
  }

  getMorePokemons() {
    for(let i=1;i<20;i++){
      this.dataService.getPokemonDataByID(i).subscribe((data: any[])=>{
          //  console.log("ASIDNAOSIJDOI", data);
        this.pokemonsData.push(data);
        })
      // this.pokemonsData.push(data)
    }
  }

  // getPokemons() {
  //   this.http.get<Pokemon[]>('http:example').subscribe(res=>{this.data=res});

  // }

}

interface Pokemon {
  name:string;
  image?:string;
  url?:string;
}
